# TP git

## À propos

Vous trouverez ici un TP git pour explorer une partie des capacités de git.

## Le TP

Le sujet est ici : https://gitlab.com/kiw/tp-git-hugo/blob/master/content/sujet.md

Le [guide de survie git](https://github.com/tclavier/memo-git/) avec les commande indispensables à connaitre. 

## Blog mis à jour à chaque nouveau push
https://kiw.gitlab.io/tp-git-hugo/

## Resources

Le livre de référence sur l'utilisation de Git https://git-scm.com/book/fr
