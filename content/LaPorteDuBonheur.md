+++
date = "2019-09-20T13:59:09+02:00"
title = "LaPorteDuBonheur"
+++

> Editeur: Un article une histoire

## Chapitre 1 : Découverte

C'est dans une prairie non loin d'ici qu'un homme, seul face à son destin, s'aventure dans la froideur de la vie.
En effet, Benoit n'était pas informé du danger present dans ce paysage, alors qu'il avance pas à pas vers l'inconnue et senti, une odeur étrange, semblable à l'odeur de framboise.

## Chapitre 2 : Exploration

There's no 'D' in Benoit... Not yet !
