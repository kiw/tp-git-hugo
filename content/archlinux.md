+++
date = "2019-09-20T15:30:54+02:00"
title = "archlinux"
draft = true

+++

# Installer Arch Linux avec XFCE *facilement*

## Prérequis

- Un ordinateur possédant un processeur `amd64` (Athlon 64/ Pentium 4 Prescott ou plus récent)
- Un support pour lancer l'installation (CD, USB, ...)
- Une connection Internet (Ethernet **fortement** recommandé)

## Installation

L'installation d'Arch Linux se caractérise par une absence totale d'interface automatique. Pour l'installer, il va donc falloir taper moultes commandes.

### Changer le layout

Au lancement, Arch Linux utilise un *layout* QWERTY, peu convivial sur les claviers français.
  
Le changement de layout se fait avec la commande suivante: `loadkeys fr-latin1`

### Connexion à l'Internet

La procédure pour se connecter en Wi-Fi est décrite [ici].
[ici]: https://wiki.archlinux.org/index.php/Wireless_network_configuration
  
Via Ethernet, la connexion est automatique. Pour vérifier le fonctionnement, faites un ping: `ping archlinux.org`

### Mettre à jour l'horloge

Linux utilise l'horloge pour un paquet de trucs. Pour la régler, tapez la commande suivante: `timedatectl set-ntp true`

### Partitionner et monter le disque d'installation

Utilisez `lsblk` pour trouver le nom du disque, puis `fdisk <le nom du disque>` pour partitionner celui-ci.
Pour formater une partition ext4, utilisez `mkfs.ext4 <partition>`.
Pour formater une partition swap, utilisez `mkswap <partition>` et `swapon <partition>`.
__
Il faut ensuite monter la partition racine du disque d'installation avec `mount <partition> /mnt`.

### Installer le système de base, et XFCE

Arch Linux permet d'effectuer ceci avec une seule commande: `pacstrap /mnt base xfce4 xfce4-goodies grub sudo lightdm lightdm-gtk-greeter xorg networkmanager` 
Pour d'autres groupes de paquets, regardez [cette liste].
[cette liste]: https://www.archlinux.org/groups/x86_64/

### Autres joyeusetés

- Générer la fstab (Automontage au démarrage) : `genfstab -U /mnt >> /mnt/etc/fstab`
- Chroot vers l'installation locale :  `arch-chroot /mnt`
- Réglage de la zone horaire : `ln -sf /usr/share/zoneinfo/*Region*/*Ville* /etc/localtime`
- Réglage de la RTC :  `hwclock --systohc`

### Choix de la langue

Décommentez la (les) lignes des langues qui vous intéressent dans `/etc/locale.gen`, puis générez les fichiers avec `locale-gen`.

Également, ajoutez:

- Dans /etc/locale.conf: `LANG=Votre locale par défaut (en_US.UTF-8)`
- Dans/etc/vconsole.conf: `KEYMAP=Le layout de votre clavier (fr-latin1)`

### Config réseau basique:

Ajoutez dans `/etc/hostname` le nom de votre machine, puis ajoutez dans `/etc/hosts`:

`
127.0.0.1	localhost
::1		localhost
127.0.1.1	myhostname.localdomain	myhostname
`

### Quelques autres joyeusetés

- Générer l'inittab: `mkinitcpio -p linux`
- Mettre un mot de passe pour root: `passwd`
- Ajouter un utilisateur et lui donner un mot de passe: `useradd -m <user> && passwd <user>`
- Créer un groupe sudo et y ajouter un utilisateur: `groupadd sudo && gpasswd -a user sudo`

### GRUB

`grub-install --target=i386-pc /dev/sdX`, où /dev/sdX représente le disque dur de l'installation.

### The Final Countdown
`
grub-mkconfig -o /boot/grub/grub.cfg &&
exit &&
reboot
`
