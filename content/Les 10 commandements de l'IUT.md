+++
date = "2019-09-20T15:54:27+02:00"
title = "Les 10 commandements de l'IUT"
draft = true
+++

# Les 10 commandements de l'IUT

## I
>  La charte informatique tu liras
<br>

## II
>  Autour du chiffre 42 ta vie tourneras
<br>

## III
>  Manger en amphi ou en salle tp tu ne feras point
<br>

## IV
>  Les tp tu finiras
<br>


## V
>  Lire les énoncés tu feras !
<br>

## VI
>  La règle "Bavarder = exclu" tu respecteras
<br>

## VII
>  La lumière du jour tu éviteras
<br>

## VIII
>  Sortir le jeudi soir tu y penseras
<br>

## IX
>  Au crous le midi tu mangeras
<br>

## X
>  Chaque semaine relir cet article tu feras
<br>














