+++
date = "2019-09-15"
title = "Sujet de TP git"

+++

# Exercices

Dans les exercices suivants, je vous invite à systématiquement observer l'état de votre espace de travail à l'aide de la commande `git status`. et à surveiller l'historique avec la commande `git log --oneline --decorate --graph --all`

Vous trouverez ici un [memo avec des explications et les commandes git](https://github.com/tclavier/memo-git/blob/master/memo.pdf) indispensables pour le bon déroulement du TP.


## Configuration

* Créer un compte sur [gitlab.com](gitlab.com)
* Sur votre machine configurer son nom, prénom et email à l'aide de la commande `config` (voir utilisez même identifiants que lors de votre inscription)
* Configurer le serveur mandataire de l'université (`http://cache.univ-lille1.fr:3128`) (voir memo)
* Configurer git pour garder en mémoire votre mot de passe pendant 3600s (voir memo)
* Vérifier à l'aide de la commande `git config --list`
* Cloner le dépot se trouvant ici https://gitlab.com/kiw/tp-git-hugo.git avec la commande `git clone`


## Les débuts (add, commit, pull, push, status)

* À l'aide de la commande hugo (cf en fin du sujet) créer un article (soyez créatifs)
* Lancez le serveur Hugo en local puis visitez  http://localhost:1313/tp-git-hugo
* Observez l'état de votre répertoire git avec `git status`
* Avec `git add <emplacement-du-fichier>` selectionnez le fichier correspondant à votre article pour l'inclure dans un futur commit.
* De nouveau observez l'état de votre répertoire git avec `git status`, 
* Vous pouvez alors faire un premier commit, utilisez un message de commit qui résume vos modifications ex : "Ajout de l'article sur xxxx".
* Observez le contenu de votre dernier commit avec `git show HEAD` et de l'historique avec `git log --oneline`
* Après validation, partagez l’article en poussant vos modifications sur le remote.
* Récupérer les articles depuis son poste avec `git pull`.
* Modifiez votre article puis jouez avec les commandes `add`, `commit`, `pull`, `push`, `status`. Décrire les différentes expériences et les résultats.


## Gestion des conflits (add, commit, pull, push)

Par équipe de 4, définir un sujet d’article et un nom de fichier associé. Pour cet exercice, vous ne modifierez que ce fichier.

* Modifier en même temps l’article de son groupe de travail.
* Partager ses modifications à plusieurs reprises.
* Expérimenter différents conflits (en haut du fichier, en bas du fichier, une ligne, de nombreuses lignes, etc.). Décrire les expériences et les résultats.

## Visualiser l’historique (blame, diff, log, show)

* Afficher l’historique des commits, dans le terminal et dans gitlab.
* Afficher la différence entre deux commits. `git diff hash-du-commit1 hash-du-commit2`
* Afficher le détail d'un commit. `git show hash-du-commit`
* Identifier l’auteur des différentes parties de son article. `git blame nom-de-larticle.md`
* Expérimenter l’affichage de l’historique avec différentes options de formatage.
* Rajouter l'alias `h` pour les options `log --oneline --decorate --graph --all` grace aux alias git

## Des changements plus évolués (mv, reset, rm)

* Renommer un article personnel et enregistrer ce changement de 2 façons différentes.
* Faire un changement et le valider avant de l'annuler définitivement.
* Ajouter un fichier, le valider puis le supprimer.

## Manipulation des branches

* Créer une branche `feature_123`
* Vérifier l’existence de cette branche
* Changer de branche
* Créer une autre branche à votre nom et se placer directement dessus.
* Supprimer la branche `feature_123`
* Comprendre et tester les commandes `checkout` et `branch`

## Fusion de branches locales et distantes

* Effectuer plusieurs commits sur la branche créée précédemment.
* Pousser cette branche sur le serveur GitLab
* Fusionner cette branche avec la branche master
* Fusionner la nouvelle branche avec la branche origin (serveur)

## Tags : Manipulation et partage

* Placer un tag sur un commit
* Pousser ce tag sur le serveur
* Récupérer et lister les tags des participants

## Remonter le temps (checkout, revert)

* Se positionner dans sa branche
* Repositionner HEAD dans un état antérieur
* Choisir un commit, puis supprimer tous les commits postérieurs à celui-ci.
* Annuler, sans les perdre, un ou plusieurs commits.

## Rebase de branche

* Créer une branche locale de type feature à votre nom
* Effectuer plusieurs commits
* Réappliquer les commits dans master via un `rebase`
* Expliquez ce que fait un `pull -r` et dans quel contexte l'utiliser.

## Mettre de côté ses développements

* Effectuer de nouvelles modifications (sans commit) sur sa branche.
* Mettre de côté son travail à l'aide de la commande `stash` et effectuer une modification (sur le même fichier).
* Une fois la modification partagé, reprendre son travail mis de côté.

## Réécrire l'histoire !

 * Prendre un commit quelconque et l’appliquer localement
 * Fusionner deux commits en un seul.
 * Changer le commentaire d’un commit

# Mémo

## Hugo

Création d'un article

    ./bin/hugo new nom-de-l-article.md

Lancer Hugo sur son poste de dev

    ./bin/hugo server --buildDrafts -w

Puis visitez http://localhost:1313/tp-git-hugo

## Markdown

    # Titre 1
    ## Sous Titre 1

    * Liste à puce, item1
    * Liste à puce, item2

    *Italique*
    **Gras**
    ***Gras et italique***


