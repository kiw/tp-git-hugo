+++
date = "2019-10-03T09:44:48+02:00"
title = "Le Grimpereau des bois"
draft = false
thumbnail = "robin.jpg"

+++


# Le Grimpereau des bois (Certhia brachydactyla) est une espèce de petits passereaux de la famille des Certhiidae.

## Petit oiseau se cachant dans les forêts, le Grimpereau des bois est souvent confondu avec une souris.


