+++
date = "2019-09-17T11:40:12+02:00"
title = "Seigneur_Des_Anneaux"
draft = true

+++

     Now it is a curious fact that this is not the story as Bilbo first told 
it to his companions. To them his account was that Gollum had promised to give
him a _present,_ if he won the game; but when Gollum went to fetch it from his
island he found the treasure was gone: a magic ring, which had been given to 
him long ago on his birthday. Bilbo guessed that this was the very ring that 
he had found, and as he had won the game, it was already his by right. But 
being in a tight place, he said nothing about it, and made Gollum show him the
way out, as a reward instead of a present. This account Bilbo set down in his 
memoirs, and he seems never to have altered it himself, not even after the 
Council of Elrond. Evidently it still appeared in the original Red Book, as it
did in several of the copies and abstracts. But many copies contain the true 
account (as an alternative), derived no doubt from notes by Frodo or Samwise, 
both of whom learned the truth, though they seem to have been unwilling to 
delete anything actually written by the old hobbit himself.
     Gandalf, however, disbelieved Bilbo's first story, as soon as he heard 
it, and he continued to be very curious about the ring. Eventually he got the 
true tale out of Bilbo after much questioning, which for a while strained 
their friendship; but the wizard seemed to think the truth important. Though 
he did not say so to Bilbo, he also thought it important, and disturbing, to 
find that the good hobbit had not told the truth from the first: quite 
contrary to his habit. The idea of a 'present' was not mere hobbitlike 
invention, all the same. It was suggested to Bilbo, as he confessed, by 
Gollum's talk that he overheard; for Gollum did, in fact, call the ring his 
'birthday present', many times. That also Gandalf thought strange and 
suspicious; but he did not discover the truth in this point for many more 
years, as will be seen in this book.
     Of Bilbo's later adventures little more need be said here. With the help 
of the ring he escaped from the orc-guards at the gate and rejoined his 
companions. He used the ring many times on his quest, chiefly for the help of 
his friends; but he kept it secret from them as long as he could. After his 
return to his home he never spoke of it again to anyone, save Gandalf and 
Frodo; and no one else in the Shire knew of its existence, or so he believed. 
Only to Frodo did he show the account of his Journey that he was writing.
     His sword, Sting, Bilbo hung over his fireplace, and his coat of 
marvellous mail, the gift of the Dwarves from the Dragon-hoard, he lent to a 
museum, to the Michel Delving Mathom-house in fact. But he kept in a drawer at
Bag End the old cloak and hood that he had worn on his travels; and the ring, 
secured by a fine chain, remained in his pocket.
     He returned to his home at Bag End on June the 22nd in his fifty-second 
year (S.R. 1342), and nothing very notable occurred in the Shire until Mr. 
Baggins began the preparations for the celebration of his hundred-and-eleventh
birthday (S.R. 1401). At this point this History begins.
     At the end of the Third Age the part played by the Hobbits in the great 
events that led to the inclusion of the Shire in the Reunited Kingdom awakened
among them a more widespread interest in their own history; and many of their 
traditions, up to that time still mainly oral, were collected and Written 
down. The greater families were also concerned with events in the Kingdom at 
large, and many of their members studied its ancient histories and legends. By
the end of the first century of the Fourth Age there were already to be found 
in the Shire several libraries that contained many historical books and 
records.
     The largest of these collections were probably at Undertowers, at Great 
Smials, and at Brandy Hall. This account of the end of the Third Age is drawn 
mainly from the Red Book of Westmarch. That most important source for the 
history of the War of the Ring was so called because it was long preserved at 
Undertowers, the home of the Fairbairns, Wardens of the Westmarch. It was in 
origin Bilbo's private diary, which he took with him to Rivendell. Frodo 
brought it back to the Shire, together with many loose leaves of notes, and 
during S.R. 1420-1 he nearly filled its pages with his account of the War. But
annexed to it and preserved with it, probably m a single red case, were the 
three large volumes, bound in red leather, that Bilbo gave to him as a parting
gift. To these four volumes there was added in Westmarch a fifth containing 
commentaries, genealogies, and various other matter concerning the hobbit 
members of the Fellowship.
     The original Red Book has not been preserved, but many copies were made, 
especially of the first volume, for the use of the descendants of the children
of Master Samwise. The most important copy, however, has a different history. 
It was kept at Great Smials, but it was written in Condor, probably at the 
request of the great-grandson of Peregrin, and completed in S.R. 1592 (F.A. 
172). Its southern scribe appended this note: Findegil, King's Writer, 
finished this work in IV 172. It is an exact copy in all details of the 
Thain's Book m Minas Tirith. That book was a copy, made at the request of King
Elessar, of the Red Book of the Periannath, and was brought to him by the 
Thain Peregrin when he retired to Gondor in IV 64.
     The Thain's Book was thus the first copy made of the Red Book and 
contained much that was later omitted or lost. In Minas Tirith it received 
much annotation, and many corrections, especially of names, words, and 
quotations in the Elvish languages; and there was added to it an abbreviated 
version of those parts of _The Tale of Aragorn and Arwen_ which lie outside 
the account of the War. The full tale is stated to have been written by 
Barahir, grandson of the Steward Faramir, some time after the passing of the 
King. But the chief importance of Findegil's copy is that it alone contains 
the whole of Bilbo's 'Translations from the Elvish'. These three volumes were 
found to be a work of great skill and learning in which, between 1403 and 
1418, he had used all the sources available to him in Rivendell, both living 
and written. But since they were little used by Frodo, being almost entirely 
concerned with the Elder Days, no more is said of them here.
     Since Meriadoc and Peregrin became the heads of their great families, and
at the same time kept up their connexions with Rohan and Gondor, the libraries
at Bucklebury and Tuckborough contained much that did not appear in the Red 
Book. In Brandy Hall there were many works dealing with Eriador and the 
history of Rohan. Some of these were composed or begun by Meriadoc himself, 
though in the Shire he was chiefly remembered for his _Herblore of the Shire,_
and for his _Reckoning of Years_ m which he discussed the relation of the 
calendars of the Shire and Bree to those of Rivendell, Gondor, and Rohan. He 
also wrote a short treatise on _Old Words and Names in the Shire,_ having 
special interest in discovering the kinship with the language of the Rohirrim 
of such 'shire-words' as _mathom_ and old elements in place names.
     At Great Smials the books were of less interest to Shire-folk, though 
more important for larger history. None of them was written by Peregrin, but 
he and his successors collected many manuscripts written by scribes of Gondor:
mainly copies or summaries of histories or legends relating to Elendil and his
heirs. Only here in the Shire were to be found extensive materials for the 
history of Númenor and the arising of Sauron. It was probably at Great Smials 
that _The Tale of Years_ was put together, with the assistance of material 
collected by Meriadoc. Though the dates given are often conjectural, 
especially for the Second Age, they deserve attention. It is probable that 
Meriadoc obtained assistance and information from Rivendell, which he visited 
more than once. There, though Elrond had departed, his sons long remained, 
together with some of the High-elven folk. It is said that Celeborn went to 
dwell there after the departure of Galadriel; but there is no record of the 
day when at last he sought the Grey Havens, and with him went the last living 
memory of the Elder Days in Middle-earth.
road; and he spoke with some authority, for he had tended the garden at Bag 
End for forty years, and had helped old Holman in the same job before that. 
Now that he was himself growing old and stiff in the joints, the job was 
mainly carried on by his youngest son, Sam Gamgee. Both father and son were on
very friendly terms with Bilbo and Frodo. They lived on the Hill itself, in 
Number 3 Bagshot Row just below Bag End.
     'A very nice well-spoken gentlehobbit is Mr. Bilbo, as I've always 
said,' the Gaffer declared. With perfect truth: for Bilbo was very polite to 
him, calling him 'Master Hamfast', and consulting him constantly upon the 
growing of vegetables – in the matter of 'roots', especially potatoes, the 
Gaffer was recognized as the leading authority by all in the neighbourhood 
(including himself).
     'But what about this Frodo that lives with him?' asked Old Noakes of 
Bywater. 'Baggins is his name, but he's more than half a Brandybuck, they say.
It beats me why any Baggins of Hobbiton should go looking for a wife away 
there in Buckland, where folks are so queer.'
     'And no wonder they're queer,' put in Daddy Twofoot (the Gaffer's next-
door neighbour), 'if they live on the wrong side of the Brandywine River, and 
right agin the Old Forest. That's a dark bad place, if half the tales be 
true.'
     'You're right, Dad!' said the Gaffer. 'Not that the Brandybucks of Buck-
land live _in_ the Old Forest; but they're a queer breed, seemingly. They fool
about with boats on that big river – and that isn't natural. Small wonder that
trouble came of it, I say. But be that as it may, Mr. Frodo is as nice a young
hobbit as you could wish to meet. Very much like Mr. Bilbo, and in more than 
looks. After all his father was a Baggins. A decent respectable hobbit was Mr.
Drogo Baggins; there was never much to tell of him, till he was drownded.'
     'Drownded?' said several voices. They had heard this and other darker 
rumours before, of course; but hobbits have a passion for family history, and 
they were ready to hear it again. 'Well, so they say,' said the Gaffer. 'You 
see: Mr. Drogo, he married poor Miss Primula Brandybuck. She was our Mr. 
Bilbo's first cousin on the mother's side (her mother being the youngest of 
the Old Took's daughters); and Mr. Drogo was his second cousin. So Mr. Frodo 
is his first _and_ second cousin, once removed either way, as the saying is, 
if you follow me. And Mr. Drogo was staying at Brandy Hall with his father-in-
law, old Master Gorbadoc, as he often did after his marriage (him being 
partial to his vittles, and old Gorbadoc keeping a mighty generous table); and
he went out _boating_on the Brandywine River; and he and his wife were 
drownded, and poor Mr. Frodo only a child and all. '
     'I've heard they went on the water after dinner in the moonlight,' said 
Old Noakes; 'and it was Drogo's weight as sunk the boat.'
     'And _I_ heard she pushed him in, and he pulled her in after him,' said 
Sandyman, the Hobbiton miller.
     'You shouldn't listen to all you hear, Sandyman,' said the Gaffer, who 
did not much like the miller. 'There isn't no call to go talking of pushing 
and pulling. Boats are quite tricky enough for those that sit still without 
looking further for the cause of trouble. Anyway: there was this Mr. Frodo 
left an orphan and stranded, as you might say, among those queer Bucklanders, 
being brought up anyhow in Brandy Hall. A regular warren, by all accounts. Old
Master Gorbadoc never had fewer than a couple of hundred relations in the 
place. Mr. Bilbo never did a kinder deed than when he brought the lad back to 
live among decent folk.
     'But I reckon it was a nasty shock for those Sackville-Bagginses. They 
thought they were going to get Bag End, that time when he went off and was 
thought to be dead. And then he comes back and orders them off; and he goes on
living and living, and never looking a day older, bless him! And suddenly he 
produces an heir, and has all the papers made out proper. The Sackville-
Bagginses won't never see the inside of Bag End now, or it is to be hoped 
not.'
     'There's a tidy bit of money tucked away up there, I hear tell,' said a 
stranger, a visitor on business from Michel Delving in the Westfarthing. 'All 
the top of your hill is full of tunnels packed with chests of gold and silver,
_and_jools, by what I've heard. '
     'Then you've heard more than I can speak to,' answered the Gaffer. I know
nothing about _jools._ Mr. Bilbo is free with his money, and there seems no 
lack of it; but I know of no tunnel-making. I saw Mr. Bilbo when he came back,
a matter of sixty years ago, when I was a lad. I'd not long come prentice to 
old Holman (him being my dad's cousin), but he had me up at Bag End helping 
him to keep folks from trampling and trapessing all over the garden while the 
sale was on. And in the middle of it all Mr. Bilbo comes up the Hill with a 
pony and some mighty big bags and a couple of chests. I don't doubt they were 
mostly full of treasure he had picked up in foreign parts, where there be 
mountains of gold, they say; but there wasn't enough to fill tunnels. But my 
lad Sam will know more about that. He's in and out of Bag End. Crazy about 
stories of the old days he is, and he listens to all Mr. Bilbo's tales. Mr. 
Bilbo has learned him his letters – meaning no harm, mark you, and I hope no 
harm will come of it.
     _'Elves and Dragons'_ I says to him. '_Cabbages and potatoes are better 
for me and you. Don't go getting mixed up in the business of your betters, or 
you'll land in trouble too big for you,'_I says to him. And I might say it to 
others,' he added with a look at the stranger and the miller.
     But the Gaffer did not convince his audience. The legend of Bilbo's 
wealth was now too firmly fixed in the minds of the younger generation of 
hobbits.
     'Ah, but he has likely enough been adding to what he brought at first,' 
argued the miller, voicing common opinion. 'He's often away from home. And 
look at the outlandish folk that visit him: dwarves coming at night, and that 
old wandering conjuror, Gandalf, and all. You can say what you like, Gaffer, 
but Bag End's a queer place, and its folk are queerer.'
     'And you can say _what you_ like, about what you know no more of than you
do of boating, Mr. Sandyman,' retorted the Gaffer, disliking the miller even 
more than usual. If that's being queer, then we could do with a bit more 
queerness in these parts. There's some not far away that wouldn't offer a pint
of beer to a friend, if they lived in a hole with golden walls. But they do 
things proper at Bag End. Our Sam says that _everyone's_ going to be invited 
to the party, and there's going to be presents, mark you, presents for all – 
this very month as is.'
     That very month was September, and as fine as you could ask. A day or two
later a rumour (probably started by the knowledgeable Sam) was spread about 
that there were going to be fireworks – fireworks, what is more, such as had 
not been seen in the Shire for nigh on a century, not indeed since the Old 
Took died.
     Days passed and The Day drew nearer. An odd-looking waggon laden with 
odd-looking packages rolled into Hobbiton one evening and toiled up the Hill 
to Bag End. The startled hobbits peered out of lamplit doors to gape at it. It
was driven by outlandish folk, singing strange songs: dwarves with long beards
and deep hoods. A few of them remained at Bag End. At the end of the second 
week in September a cart came in through Bywater from the direction of the 
Brandywine Bridge in broad daylight. An old man was driving it all alone. He 
wore a tall pointed blue hat, a long grey cloak, and a silver scarf. He had a 
long white beard and bushy eyebrows that stuck out beyond the brim of his hat.
Small hobbit-children ran after the cart all through Hobbiton and right up the
hill. It had a cargo of fireworks, as they rightly guessed. At Bilbo's front 
door the old man began to unload: there were great bundles of fireworks of all
sorts and shapes, each labelled with a large red G  and the elf-rune, .
     That was Gandalf's mark, of course, and the old man was Gandalf the 
Wizard, whose fame in the Shire was due mainly to his skill with fires, 
smokes, and lights. His real business was far more difficult and dangerous, 
but the Shire-folk knew nothing about it. To them he was just one of the 
'attractions' at the Party. Hence the excitement of the hobbit-children. 'G 
for Grand!' they shouted, and the old man smiled. They knew him by sight, 
though he only appeared in Hobbiton occasionally and never stopped long; but 
neither they nor any but the oldest of their elders had seen one of his 
firework displays – they now belonged to the legendary past.
     When the old man, helped by Bilbo and some dwarves, had finished 
unloading. Bilbo gave a few pennies away; but not a single squib or cracker 
was forthcoming, to the disappointment of the onlookers.
     'Run away now!' said Gandalf. 'You will get plenty when the time comes.' 
Then he disappeared inside with Bilbo, and the door was shut. The young 
hobbits stared at the door in vain for a while, and then made off, feeling 
that the day of the party would never come.
     Inside Bag End, Bilbo and Gandalf were sitting at the open window of a 
small room looking out west on to the garden. The late afternoon was bright 
and peaceful. The flowers glowed red and golden: snap-dragons and sun-flowers,
and nasturtiums trailing all over the turf walls and peeping in at the round 
windows.
     'How bright your garden looks!' said Gandalf.
     'Yes,' said Bilbo. I am very fond indeed of it, and of all the dear old 
Shire; but I think I need a holiday.'
     'You mean to go on with your plan then?'
     'I do. I made up my mind months ago, and I haven't changed it.'
     'Very well. It is no good saying any more. Stick to your plan – your 
whole plan, mind – and I hope it will turn out for the best, for you, and for 
all of us.'
     'I hope so. Anyway I mean to enjoy myself on Thursday, and have my little
joke.'
     'Who will laugh, I wonder?' said Gandalf, shaking his head.
     'We shall see,' said Bilbo.
     The next day more carts rolled up the Hill, and still more carts. There 
might have been some grumbling about 'dealing locally', but that very week 
orders began to pour out of Bag End for every kind of provision, commodity, or
luxury that could be obtained in Hobbiton or Bywater or anywhere in the 
neighbourhood. People became enthusiastic; and they began to tick off the days
on the calendar; and they watched eagerly for the postman, hoping for 
invitations.
     Before long the invitations began pouring out, and the Hobbiton post-
office was blocked, and the Bywater post-office was snowed under, and 
voluntary assistant postmen were called for. There was a constant stream of 
them going up the Hill, carrying hundreds of polite variations on _Thank you, 
I shall certainly come._
     A notice appeared on the gate at Bag End: NO ADMITTANCE EXCEPT ON PARTY 
BUSINESS. Even those who had, or pretended to have Party Business were seldom 
allowed inside. Bilbo was busy: writing invitations, ticking off answers, 
packing up presents, and making some private preparations of his own. From the
time of Gandalf's arrival he remained hidden from view.
     One morning the hobbits woke to find the large field, south of Bilbo's 
front door, covered with ropes and poles for tents and pavilions. A special 
entrance was cut into the bank leading to the road, and wide steps and a large
white gate were built there. The three hobbit-families of Bagshot Row, 
adjoining the field, were intensely interested and generally envied. Old 
Gaffer Gamgee stopped even pretending to work in his garden.
     The tents began to go up. There was a specially large pavilion, so big 
that the tree that grew in the field was right inside it, and stood proudly 
near one end, at the head of the chief table. Lanterns were hung on all its 
branches. More promising still (to the hobbits' mind): an enormous open-air 
kitchen was erected in the north corner of the field. A draught of cooks, from
every inn and eating-house for miles around, arrived to supplement the dwarves
and other odd folk that were quartered at Bag End. Excitement rose to its 
height.
     Then the weather clouded over. That was on Wednesday the eve of the 
Party. Anxiety was intense. Then Thursday, September the 22nd, actually 
dawned. The sun got up, the clouds vanished, flags were unfurled and the fun 
began.
     Bilbo Baggins called it a _party,_ but it was really a variety of 
entertainments rolled into one. Practically everybody living near was invited.
A very few were overlooked by accident, but as they turned up all the same, 
that did not matter. Many people from other parts of the Shire were also 
asked; and there were even a few from outside the borders. Bilbo met the 
guests (and additions) at the new white gate in person. He gave away presents 
to all and sundry – the latter were those who went out again by a back way and
came in again by the gate. Hobbits give presents to other people on their own 
birthdays. Not very expensive ones, as a rule, and not so lavishly as on this 
occasion; but it was not a bad system. Actually in Hobbiton and Bywater every 
day in the year it was somebody's birthday, so that every hobbit in those 
parts had a fair chance of at least one present at least once a week. But they
never got tired of them.
     On this occasion the presents were unusually good. The hobbit-children 
were so excited that for a while they almost forgot about eating. There were 
toys the like of which they had never seen before, all beautiful and some 
obviously magical. Many of them had indeed been ordered a year before, and had
come all the way from the Mountain and from Dale, and were of real dwarf-make.
     When every guest had been welcomed and was finally inside the gate, there
were songs, dances, music, games, and, of course, food and drink. There were 
three official meals: lunch, tea, and dinner (or supper). But lunch and tea 
were marked chiefly by the fact that at those times all the guests were 
sitting down and eating together. At other times there were merely lots of 
people eating and drinking – continuously from elevenses until six-thirty, 
when the fireworks started.
     The fireworks were by Gandalf: they were not only brought by him, but 
designed and made by him; and the special effects, set pieces, and flights of 
rockets were let off by him. But there was also a generous distribution of 
squibs, crackers, backarappers, sparklers, torches, dwarf-candles, elf-
fountains, goblin-barkers and thunder-claps. They were all superb. The art of 
Gandalf improved with age.
     There were rockets like a flight of scintillating birds singing with 
sweet voices. There were green trees with trunks of dark smoke: their leaves 
opened like a whole spring unfolding in a moment, and their shining branches 
dropped glowing flowers down upon the astonished hobbits, disappearing with a 
sweet scent just before they touched their upturned faces. There were 
fountains of butterflies that flew glittering into the trees; there were 
pillars of coloured fires that rose and turned into eagles, or sailing ships, 
or a phalanx of flying swans; there was a red thunderstorm and a shower of 
yellow rain; there was a forest of silver spears that sprang suddenly into the
air with a yell like an embattled army, and came down again into the Water 
with a hiss like a hundred hot snakes. And there was also one last surprise, 
in honour of Bilbo, and it startled the hobbits exceedingly, as Gandalf 
intended. The lights went out. A great smoke went up. It shaped itself like a 
mountain seen in the distance, and began to glow at the summit. It spouted 
green and scarlet flames. Out flew a red-golden dragon – not life-size, but 
terribly life-like: fire came from his jaws, his eyes glared down; there was a
roar, and he whizzed three times over the heads of the crowd. They all ducked,
and many fell flat on their faces. The dragon passed like an express train, 
turned a somersault, and burst over Bywater with a deafening explosion.
     'That is the signal for supper!' said Bilbo. The pain and alarm vanished 
at once, and the prostrate hobbits leaped to their feet. There was a splendid 
supper for everyone; for everyone, that is, except those invited to the 
special family dinner-party. This was held in the great pavilion with the 
tree. The invitations were limited to twelve dozen (a number also called by 
the hobbits one Gross, though the word was not considered proper to use of 
people); and the guests were selected from all the families to which Bilbo and
Frodo were related, with the addition of a few special unrelated friends (such
as Gandalf). Many young hobbits were included, and present by parental 
permission; for hobbits were easy-going with their children in the matter of 
sitting up late, especially when there was a chance of getting them a free 
meal. Bringing up young hobbits took a lot of provender.
     There were many Bagginses and Boffins, and also many Tooks and 
Brandybucks; there were various Grubbs (relations of Bilbo Baggins' 
grandmother), and various Chubbs (connexions of his Took grandfather); and a 
selection of Burrowses, Bolgers, Bracegirdles, Brockhouses, Goodbodies, 
Hornblowers and Proudfoots. Some of these were only very distantly connected 
with Bilbo, and some of them had hardly ever been in Hobbiton before, as they 
lived in remote corners of the Shire. The Sackville-Bagginses were not 
forgotten. Otho and his wife Lobelia were present. They disliked Bilbo and 
detested Frodo, but so magnificent was the invitation card, written in golden 
ink, that they had felt it was impossible to refuse. Besides, their cousin, 
Bilbo, had been specializing in food for many years and his table had a high 
reputation.
     All the one hundred and forty-four guests expected a pleasant feast; 
though they rather dreaded the after-dinner speech of their host (an 
inevitable item). He was liable to drag in bits of what he called poetry; and 
sometimes, after a glass or two, would allude to the absurd adventures of his 
mysterious journey. The guests were not disappointed: they had a _very_ 
pleasant feast, in fact an engrossing entertainment: rich, abundant, varied, 
and prolonged. The purchase of provisions fell almost to nothing throughout 
the district in the ensuing weeks; but as Bilbo's catering had depleted the 
stocks of most stores, cellars and warehouses for miles around, that did not 
matter much.
     After the feast (more or less) came the Speech. Most of the company were,
however, now in a tolerant mood, at that delightful stage which they called 
'filling up the corners'. They were sipping their favourite drinks, and 
nibbling at their favourite dainties, and their fears were forgotten. They 
were prepared to listen to anything, and to cheer at every full stop.
     _My dear People,_ began Bilbo, rising in his place. 'Hear! Hear! Hear!' 
they shouted, and kept on repeating it in chorus, seeming reluctant to follow 
their own advice. Bilbo left his place and went and stood on a chair under the
illuminated tree. The light of the lanterns fell on his beaming face; the 
golden buttons shone on his embroidered silk waistcoat. They could all see him
standing, waving one hand in the air, the other was in his trouser-pocket.
     _My dear Bagginses and Boffins,_ he began again; _and my dear Tooks and 
Brandybucks, and Grubbs, and Chubbs, and Burrowses, and Hornblowers, and 
Bolgers, Bracegirdles, Goodbodies, Brockhouses and Proudfoots._ 'ProudFEET!' 
shouted an elderly hobbit from the back of the pavilion. His name, of course, 
was Proudfoot, and well merited; his feet were large, exceptionally furry, and
both were on the table.
     _Proudfoots,_ repeated Bilbo. _Also my good Sackville-Bagginses that I 
welcome back at last to Bag End. Today is my one hundred and eleventh 
birthday: I am eleventy-one today!_ 'Hurray! Hurray! Many Happy Returns!' they
shouted, and they hammered joyously on the tables. Bilbo was doing splendidly.
This was the sort of stuff they liked: short and obvious.
     _I hope you are all enjoying yourselves as much as I am._ Deafening 
cheers. Cries of _Yes_ (and _No)._ Noises of trumpets and horns, pipes and 
flutes, and other musical instruments. There were, as has been said, many 
young hobbits present. Hundreds of musical crackers had been pulled. Most of 
them bore the mark DALE on them; which did not convey much to most of the 
hobbits, but they all agreed they were marvellous crackers. They contained 
instruments, small, but of perfect make and enchanting tones. Indeed, in one 
corner some of the young Tooks and Brandybucks, supposing Uncle Bilbo to have 
finished (since he had plainly said all that was necessary), now got up an 
impromptu orchestra, and began a merry dance-tune. Master Everard Took and 
Miss Melilot Brandybuck got on a table and with bells in their hands began to 
dance the Springle-ring: a pretty dance, but rather vigorous.
     But Bilbo had not finished. Seizing a horn from a youngster near by, he 
blew three loud hoots. The noise subsided. _I shall not keep you long,_ he 
cried. Cheers from all the assembly. _I have called you all together for a 
Purpose._ Something in the way that he said this made an impression. There was
almost silence, and one or two of the Tooks pricked up their ears.
     _Indeed, for Three Purposes! First of all, to tell you that I am 
immensely fond of you all, and that eleventy-one years is too short a time to 
live among such excellent and admirable hobbits._ Tremendous outburst of 
approval.
     _I don't know half of you half as well as I should like; and I like less 
than half of you half as well as you deserve._ This was unexpected and rather 
difficult. There was some scattered clapping, but most of them were trying to 
work it out and see if it came to a compliment.
     _Secondly, to celebrate my birthday._ Cheers again. _I should say: OUR 
birthday. For it is, of course, also the birthday of my heir and nephew, 
Frodo. He comes of age and into his inheritance today._ Some perfunctory 
clapping by the elders; and some loud shouts of 'Frodo! Frodo! Jolly old 
Frodo,' from the juniors. The Sackville-Bagginses scowled, and wondered what 
was meant by 'coming into his inheritance'. _Together we score one hundred and
forty-four. Your numbers were chosen to fit this remarkable total: One Gross, 
if I may use the expression._ No cheers. This was ridiculous. Many of his 
guests, and especially the Sackville-Bagginses, were insulted, feeling sure 
they had only been asked to fill up the required number, like goods in a 
package. 'One Gross, indeed! Vulgar expression.'
     _It is also, if I may be allowed to refer to ancient history, the 
anniversary of my arrival by barrel at Esgaroth on the Long Lake; though the 
fact that it was_ my _birthday slipped my memory on that occasion. I was only 
fifty-one then, and birthdays did not seem so important. The banquet was very 
splendid, however, though I had a bad cold at the time, I remember, and could 
only say 'thag you very buch'. I now repeat it more correctly: Thank you very 
much for coming to my little party._ Obstinate silence. They all feared that a
song or some poetry was now imminent; and they were getting bored. Why 
couldn't he stop talking and let them drink his health? But Bilbo did not sing
or recite. He paused for a moment.
     _Thirdly and finally,_ he said, _I wish to make an ANNOUNCEMENT_. He 
spoke this last word so loudly and suddenly that everyone sat up who still 
could. _I regret to announce that – though, as I said, eleventy-one years is 
far too short a time to spend among you – this is the END. I am going. I am 
leaving NOW. GOOD-BYE!
