+++
date = "2019-09-20T14:07:20+02:00"
title = "automne"
draft = true
+++


les sanglots longs des violons de l'automne blessent mon coeur d'une langueur monotone tout suffocant et blême quand sonne l'heure je me souviens des jours anciens et je pleure et je m'en vais au vent mauvais qui méemporte deça dela pareil à la feuille morte. 