+++
date = "9999-12-31T23:59:59-99:59"
title = "              La calvitie à travers les âges"
thumbnail = "jason.jpg"

+++

![Laboule](https://resize-parismatch.lanmedia.fr/r/625,417,center-middle,ffffff/img/var/news/storage/images/paris-match/people/dwayne-the-rock-johnson-papa-pour-la-troisieme-fois-1502840/24579974-1-fre-FR/Dwayne-The-Rock-Johnson-papa-pour-la-troisieme-fois.jpg)
![Laboule](https://cdn-elle.ladmedia.fr/var/plain_site/storage/images/people/la-vie-des-people/news/fort-boyard-yves-marchesseau-alias-la-boule-est-decede-2826096/50859858-1-fre-FR/Fort-Boyard-Yves-Marchesseau-alias-La-Boule-est-decede.jpg)
![Jason](http://fr.web.img5.acsta.net/pictures/19/07/31/17/35/5396784.jpg)
![Bruce](https://upload.wikimedia.org/wikipedia/commons/c/c4/Bruce_Willis_by_Gage_Skidmore_3.jpg)





En dermatologie, l'alopécie désigne l'accélération de la chute des cheveux ou des poils. Le terme vient du grec ancien ἀλωπεκία / alôpekía (« chute des cheveux ») dérivé de ἀλώπηξ / alốpêx (« renard ») par analogie avec la chute annuelle des poils d'hiver de cet animal au début du printemps. On parle généralement d'alopécie lorsque cette perte de cheveux dépasse 100 cheveux par jour sur une longue période. La calvitie (état d'une personne chauve) est le développement ultime de l'alopécie.

Histoire de la calvitie

La calvitie fut un sujet de préoccupation pour les hommes, constant au cours de l'histoire. Une histoire talmudique (parasha Kora'h) allégorique évoque une conversation entre un sage chauve et un eunuque chevelu1. De l'Antiquité à nos jours, les hommes ont recouru à divers subterfuges pour la camoufler. Dès l'Antiquité, des traitements empiriques et naturels sont utilisés. On peut citer notamment la salive de cheval, le fumier d'oie ou les toiles d'araignée appliquées sur le crâne et qui étaient censées faire repousser les cheveux2.

Au XIXe siècle, le filon est largement exploité par la « médecine de troisième vitesse », au sein de laquelle se trouvent ce que l'on appelle dans le langage courant les charlatans.

Dans la mythologie grecque, pour venir en aide à Laomédon, le roi de Troie, et sauver sa fille Hésione, Héraclès en affrontant Céto vient à perdre ses cheveux3,4.
Physiologie
Perte de cheveux normale et anormale

Une personne possède en moyenne 100 000 à 150 000 cheveux et perd environ 40 à 100 cheveux par jour (chute permanente), avec des pointes pouvant grimper jusqu'à 175 durant les changements de saison (chute saisonnière le printemps et l'automne). C'est durant cette phase que les cheveux se renouvellent le plus. Une chute de cheveux est considérée comme pathologique lorsqu'une personne perd plus de 100 cheveux par jour pendant une assez longue période pouvant aller jusqu'à deux mois. Le diagnostic de cette chute nécessite une consultation et parfois un examen appelé « trichogramme » (on prend quelques cheveux dans différentes zones du cuir chevelu et on les observe au microscope). On parle de calvitie lorsque la chute est localisée sur le front, au dessus des tempes et au sommet du crâne. Parfois, un bilan hormonal est nécessaire chez la femme. Le résultat de ces examens oriente vers l'un des problèmes suivants5.
Classification
L'alopécie areata.

On distingue cinq types d'alopécies :

l'alopécie androgénétique héréditaire
    La plus fréquente, elle se manifeste par une diminution du volume des cheveux, voire une calvitie, et touche 70 % des hommes (20 % des hommes entre 20 et 30 ans, la chute de cheveux débutant généralement vers 20 ans et se stabilisant à 30 ans6).
l'alopécie aiguë
    Elle peut être liée à un traitement par chimiothérapie, un stress, des carences alimentaires importantes, une carence en fer, des troubles hormonaux, une irradiation aiguë ;
l'alopécie localisée
    Elle peut être provoquée par des problèmes de peau (tumeur, brûlure, pelade), une radiothérapie ou des parasites (teigne, lichen) ;
l'alopécie congénitale
l'alopécie areata
    L'alopécie areata semble être d'origine auto-immune (mécanisme de médiation cellulaire) qui se caractérise par une atteinte en « patch » plus ou moins gros et à un ou plusieurs endroits. Cette forme de pelade peut atteindre toute la tête et on parle d'alopécie totalis et parfois l'ensemble du corps : c'est l'alopécie universalis, et dans ce cas, il n'y a plus aucun poil ni cheveu sur l'ensemble du corps.

Les phases du cycle pilaire
Phase anagène

La phase anagène est la phase de croissance du cheveu. Il s’agit de la période la plus longue du cycle pilaire puisqu'elle dure de 2 à 5 ans. La très grande majorité des cheveux présents sur notre tête sont donc en phase anagène.
Phase catagène

La phase catagène est une phase de repos pendant laquelle le cheveu cesse d'évoluer. Elle dure environ 3 semaines, ce qui est considérablement peu par rapport à la phase précédente.
Phase télogène

Pendant la phase télogène, le cheveu ne pousse pas, mais il reste attaché au follicule pileux. À la fin de cette phase, l'ancien cheveu tombe et laisse place à un nouveau follicule en phase anagène, le cycle pilaire recommence7.
Causes
Causes hormonalesine.

L'alopécie androgénétique e cas il engendre une stabilisation de la chute et dans 60 %, une repousse plus ou moins importante. Les femmes enceintes ne devraient jamais manipuler un comprimé de Propécia, car le produit a été reconnu comme étant responsable de malformations chez les fœtus mâles21,22.

Les effets secondaires du Propécia sur le long terme sont encore méconnus ;

    Ils sont en général temporaires et réversibles à l'arrêt du traitement, mais pas toujours.
    Des troubles sexuels permanents ont été documentés. Les effets secondaires les plus souvent associés à la prise de finastéride sont une baisse de la libido, des problèmes d'érection, une diminution du volume de l'éjaculat, une modifica de Propecia par la femme enceinte sont citées et ce médicament peut affecter les organes internes de la femme (et du fœtus)23.
    Des études plus récentes ont montré que Propécia pouvaient favoriser la dépression et d'éventuelles tendances suicidaires.

Le finastéride ne doit pas être pris par les femmes ou les enfants.

Il peut être absorbé par la peau (passage transcutané).
Minoxidil

Le minoxidil est le premier médicament commercialisé pour traiter la calvitie, sous le nom de Rogaine, et sous plusieurs autres noms en version générique (Alostil, Kirkland, etc.). Son action est inconnue, mais certains chercheurs estiment que ce médicament dilaterait les vaisseaux sanguins, ce qui entrainerait une plus grande irrigation sanguine du cuir chevelu et amènerait plus de nutriments à la racine des cheveux. Il doit être appliqué sur le cuir chevelu 2 fois par jour et lors de l'arrêt de son application, les cheveux obtenus grâce au produit tombent. Deux dosages sont disponibles: en version 2 % et 5 %. Seule la version 2 % est utilisable par les femmes. Les hommes peuvent l'utiliser, mais comparativement à la version 5 %, ses effets sur la repousse et le maintien des cheveux sont beaucoup plus faibles.
Dutastéride

Tout comme le finastéride et le minoxidil, le dutastéride (en) n'était pas destiné à remédier à la calvitie androgénétique, mais plutôt à soigner l'hypertrophie bénigne de la prostate. Ce médicament serait 1,5 fois plus puissant que le Propécia dans la réduction de la DHT (chiffre à confirmer, car il n'est pas accepté par aucu à ceux du Propécia, tout en étant aussi plus fort à cause de la réduction importante de l'hormone DHT. Les pourcentages de personnes atteintes par un effet secondaire doubleraient par rapport au Propécia.
Médecine esthétique
Micro-greffe
Article détaillé : Greffe de cheveux.

Cette technique consiste à prélever des greffons de cheveux sur la couronne (ou les cheveux éternels) et les greffer sur le haut de la tête. Cette technique est la seule qui puisse se vanter d'être une solution définitive au problème de perte de cheveux, car les cheveux greffés ne vont, en général, jamais tomber. Malheureusement, cette technique n'est pas efficace pour regarnir les crânes complètement, car il n'y pas assez de cheveux sur la couronne pour fournir une tête entière. De plus, il s'agit d'une technique qui compte beaucoup sur l'habilité du médecin qui va pratiquer la greffe. En effet, il s'agit d'une opération délicate, car si les greffons sont placés dans un mauvais sens, le patient risque de se retrouver avec des cheveux poussant dans tous les sens. Aujourd'hui les médecins peuvent être assistés par des robots lors du prélèvement des greffons24.
LLLT : Low Level Laser Therapy, thérapie par laser de basse énergie

La LLLT, Low Level Laser Therapy, « thérapie par laser de basse énergie », agit à la manière d'une biostimulation. Des tests ont été effectués chez des souris, menant au constat que la pousse du poil est favorisée. Étant donné le prix des diodes, une nouvelle tendance née aux États-Unis consiste à fabriquer soi-même son casque laser25,26. Le temps nécessaire à l'utilisation de cette thérapie est estimé à trois fois 20 minutes par semaine. Les utilisateurs du minoxidil constateront des effets moindres par rapport à ceux qui ne l'utilisent pas.
Cosmétiques
Poudres et vaporisateurs

Des poudres et des vaporisateurs sont utilisés dans le but de camoufler la calvitie. Le principe de base de ces méthodes est l'augmentation de l'épaisseur des cheveux. Ces produits étant parfois colorés, ils permettent également de masquer les racines sur cheveux colorés27.
Perruques ou compléments

La perruque est de nos jours un outil de premier plan pour lutter contre la calvitie et surtout les pertes de cheveux excessives, comme celles causées par une chimiothérapie ou une pelade par exemple. Il existe de nos jours, en plus des traditionnelles perruques en cheveux synthétiques, des perruques en cheveux naturels.

Les compléments capillaires sont des prothèses visant à camoufler la perte de cheveux, que l'alopécie soit diffuse ou plus importante, voire complète sur certaines zones. Le volumateur est par exemple plus adapté aux femmes qui ont généralement une chute de cheveux diffuse. Il s'agit d'un filet sur lequel sont fixés des cheveux. Ce filet se fixe aux moyens de diverses techniques (tissage, colle, clip) sur les cheveux restants du malade28.

Les cheveux des compléments capillaires sont quant à eux implantés soit par nœuds dans une matière appelée Tulle ou Lace soit injectés dans un copolymère polyuréthane parfois appelé micro ou nano peau. L'adhésion de la prothèse peut être effectuée par colle capillaire, bande adhésive, clip, tresse ou par couture. Chez les hommes de nombreuses stars internationales sont porteuses de ce système, John Travolta en est un précurseur et les utilise depuis plus de deux décennies.
Micro-pigmentation du cuir chevelu (hair tattoo)
Le principe de cette micro-pigmentation, appelée aussi micro hair tattoo (MHT), est d’injecter une faible quantité de pigment dans le cuir chevelu à l'aide d'un dermographe chirurgical et d'imiter ainsi une chevelure rasée de près29. 
