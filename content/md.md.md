+++
title = "md.md"
draft = true
date = "2019-09-20T16:21:10+02:00"
+++


La framboise est un fruit rouge issu du framboisier (Rubus idaeus), un arbrisseau de la famille des rosacées. Selon qu'il s'agit de framboisiers sauvages ou cultivés, la framboise pèse de 4 à 10 g, mesure jusqu'à 2,5 cm et compte de 40 à 80 drupéoles1. 


## Table of contents

- [Quick start](#quick-start)
- [Status](#status)
- [What's included](#whats-included)
- [Bugs and feature requests](#bugs-and-feature-requests)
- [Contributing](#contributing)
- [Creators](#creators)
- [Thanks](#thanks)
- [Copyright and license](#copyright-and-license)


## Quick start

Some text -- OHLOLOLOLOL

- Instruction 1
- Instruction 2
- Instruction 3

## Status

Here goes all the budgets

## What's included

Some text

```text
folder1/
└── folder2/
    ├── folder3/
    │   ├── file1
    │   └── file2
    └── folder4/
        ├── file3
        └── file4
```

## Bugs and feature requests

Have a bug or a feature request? Please first read the [issue guidelines](https://reponame/blob/master/CONTRIBUTING.md) and search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://reponame/issues/new).

## Contributing

Please read through our [contributing guidelines](https://reponame/blob/master/CONTRIBUTING.md). Included are directions for opening issues, coding standards, and notes on development.

Moreover, all HTML and CSS should conform to the [Code Guide](https://github.com/mdo/code-guide), maintained by [Main author](https://github.com/usernamemainauthor).

Editor preferences are available in the [editor config](https://reponame/blob/master/.editorconfig) for easy use in common text editors. Read more and download plugins at <https://editorconfig.org/>.

## Creators

**Creator 1**

- <https://github.com/usernamecreator1>

## Thanks

Some Text

## Copyright and license

Code and documentation copyright 2011-2018 the authors. Code released under the [MIT License](https://reponame/blob/master/LICENSE).

Enjoy :metal:

<<<<<<< HEAD


Wiki Loves Monuments : photographiez un monument historique, aidez Wikipédia et gagnez !
En apprendre plus
Gondole
Sauter à la navigationSauter à la recherche
Page d'aide sur l'homonymie Pour les articles homonymes, voir Gondole (homonymie).

Gondoles
La gondole (gondola en italien) est une barque de couleur noire à une rame utilisée dans la ville de Venise en Italie.


Sommaire
1	Histoire
2	Description
3	Gondoliers
4	Gondolino
5	Barchéta da traghéto
6	Représentation dans les arts
7	Notes et références
8	Voir aussi
8.1	Bibliographie
8.2	Articles connexes 
8.3	Liens externes
Histoire
Le nom de gondole est mentionné pour la première fois dans un décret du doge Vital Faliero de Doni en 1094. Mais son aspect actuel remonte en partie au xvie siècle. Un décret dogal de 1562, imposa la couleur noire afin de mettre un terme à la compétition ruineuse qui opposait les riches vénitiens, ambitieux de posséder l’embarcation la plus richement décorée. La légende attribuant le choix de cette couleur à la commémoration des pestes est donc sans fondement. Avant cette réglementation, la gondole était menée par deux rameurs et ne se distinguait pas des autres embarcations vénitiennes hormis le fait qu’elle était utilisée comme moyen de transport privé de personnes. Ainsi, jusqu’au début de la Seconde Guerre mondiale, les patriciens et les riches commerçants de la ville, engageaient des gondoliers pour leur service personnel.

Description
La gondole utilisée à Venise au début du xxie siècle est constituée de 280 morceaux de bois (chêne mélèze, noyer, cerisier, tilleul, cèdre et contreplaqué) et de deux pièces métalliques situées à la proue et à la poupe. L’embarcation mesure 10,80 mètres de long et 1,38 mètre de large pour un poids de 600 kilogrammes. Basse et légère pour être maniable, elle est propulsée par un seul rameur qui se tient debout à l’arrière gauche en ramant du côté droit, d’où l’asymétrie de la gondole, modification introduite au xixe siècle. L’axe transversal est ainsi décalé vers la droite pour tenir compte du poids du gondolier tandis que le côté gauche est plus courbé afin de garder une trajectoire droite.


Le ferro de prua
L’unique rame est en bois indonésien et mesure 4,20 mètres. Plate, elle n’est pas fixée, ce qui permet de la dégager rapidement, et s’appuie simplement sur la forcola (it), pièce en bois généralement constituée de noyer, coupée dans un seul morceau de bois et taillée selon les mensurations du gondolier. Les morsi (mors), ces huit échancrures arrondies, sont utilisés pour ramer. Chacun est utilisé pour une manœuvre précise (marche avant, marche arrière, virage court, rotation sur place). La bougie placée à l'opposé du gondolier sert à éclairer la nuit et apporte une note de romantisme. Le cavali (« cheval »), ornement à mi-longueur de la gondole au niveau des accoudoirs, représente des figures allégoriques comme un hippocampes ou une sirène.

Il fero de prua (terme vénitien pour désigner la figure de proue de la gondole) était à l’origine utilisé pour contrebalancer le poids du gondolier. Au cours du xviie siècle, il acquit une symbolique précise. Les six barres horizontales parallèles symbolisent les six sestieri (« quartiers ») de Venise et la barre située en arrière l’île de la Giudecca. La courbure symbolise quant à elle le Grand Canal. La grande partie en haut du ferro représente le chapeau d'un doge ( autrefois dirigeant de la ville de Venise ). Enfin, l’espace vide formé par la rencontre de la figure supérieure et de la première barre représente le Pont du Rialto. Il est toujours blanc.

Les squeri (sing. squero, du grec ἐσχάριον (eschárion), chantier naval ou de squa(d)ra, outil de charpentier) construisent les gondoles tandis que les remeri fabriquent les rames et sculptent les forcola. Il faut actuellement environ un mois pour fabriquer une gondole, dont le modèle standard coûte 20 000 euros.

Gondoliers

Gondoliers dans le bacino Orseolo, point d'arrêt des gondoles

Le Doge de Venise porté par les gondoliers, après son élection sur la place Saint-Marc
par Francesco Guardi vers 1770
musée de Grenoble1
Aujourd’hui la gondole n’est plus utilisée que par les touristes. Si aujourd’hui[Quand ?] 433 gondoliers ont un permis de navigation, il ne peut y en avoir plus. On a estimé à 10 000 le nombre de gondoles au xvie siècle. Les gondoliers constituaient autrefois une caste à part, qui voulait qu'un père transmette les secrets de son savoir à l’un de ses fils.

Ce système a disparu depuis 1980 et le gondolier est désormais sélectionné sur concours ouvert à tous. À l’examen il faut non seulement prouver sa science de l’aviron, son sérieux pour l’entretien de la barque, mais également sa manière diligente et courtoise d’accueillir les touristes et de les aider à s’installer confortablement. La connaissance de langues étrangères est bien évidemment un plus.

En 2009, une première femme a obtenu l'autorisation de conduire une gondole, elle officie dans le quartier de Dorsoduro.

L’une des ambitions du gondolier est de participer un jour au championnat annuel : gagner le concours et appartenir à une famille qui comprend des lauréats est très important. La plus grande course annuelle est la Regata Storica. S’entraîner pour la gagner est un souci constant car les concurrents sont très nombreux. Aligner deux ou trois succès classe le jeune rameur parmi les plus solides réputations. La course atteint cinq kilomètres, qu’il faut souvent parcourir en pleine chaleur.

Gondolino
Le gondolino fut créé et exclusivement utilisé pour la Regata Storica. Apparu lors de la régate de 1825, il servait à élever l'esprit de compétition et à rendre la régate plus passionnante. Il est plus léger et rapide que la gondole, dont il tire sa forme. Il est long de 10,50 m, large de 1,10 m et le fond atteint 0,65 m en largeur.

Barchéta da traghéto

Barchéta da traghetto sur le Grand Canal
Ce bateau appartient à la famille des gondoles. Il est moins asymétrique, sans la poupe et l'arc caractéristiques remplacés par une simple lame, plus lourde et plus large que la gondole classique. Il était utilisé comme bac à l'intérieur ou vers l'extérieur de la ville vers le Lido et Mestre, en tant que bateau omnibus après 1848 pour le transport vers la gare (partant du Ponte de la Paglia), pour le transport des malades et le transport les corps au cimetière.
Actuellement, quatre lignes de bac piétons sont encore actives sur le Grand Canal (Venise): à Santa Sofia, San Samuele, San Gregorio et San Tomà.

Représentation dans les arts
L'écrivaine française George Sand publie en 1837-1838 un roman, La Dernière Aldini, dont le personnage principal, Lélio, est un chanteur professionnel qui exerce longtemps le métier de gondolier à Venise.

En musique, le compositeur Franz Liszt compose en 1840 une pièce pour piano intitulée La Gondoliera.

Le film Le Gondolier de Broadway, une comédie musicale américaine réalisée par Lloyd Bacon en 1935, relate l'histoire d'un chauffeur de taxi qui arrive à Venise où il se fait passer pour un gondolier afin de devenir chanteur à la radio.

Notes et références
Base Joconde [archive]
Voir aussi
Bibliographie
Gondole, symbole de Venise C. Parvulesco, ETAI 2007
Articles connexes 
squero
(1891) Gondola, astéroïde.
Liens externes
Les gondoles sur Commons Les traghetti sur Commons
 Portail de la mer Adriatique  Portail de Venise  Portail du monde maritime
Catégories : Transport à VeniseBateau à passagersType de bateauxBateau vénitienMer AdriatiqueVocabulaire maritime[+]
=======
-------CROCHET------

Wiki Loves Monuments : photographiez un monument historique, aidez Wikipédia et gagnez ! En apprendre plus
Crochet
Sauter à la navigation
Sauter à la recherche

Sur les autres projets Wikimedia :

    crochet, sur le Wiktionnaire 

Page d'aide sur l'homonymie. Cette page d’homonymie répertorie les différents sujets et articles partageant un même nom.

Un crochet est au sens propre un objet que sa courbure rend apte à la préhension. Par analogie le mot prend des sens variés, où reste présente la notion de forte courbure.
Crochet de bricolage.
Équerre.
Sommaire

    1 Outils
        1.1 Outils pour suspendre
        1.2 Outils textiles
        1.3 Autres outils
    2 Sciences et techniques
        2.1 Biologie
        2.2 Mathématiques
        2.3 Typographie
    3 Culture et société
        3.1 Langue courante
        3.2 Légendes et littérature
        3.3 Artisanat
        3.4 Radiophonie
        3.5 Sports et arts martiaux
    4 Patronyme
    5 Notes et références

Outils

Différents objets servant à accrocher ou suspendre sont appelés crochets.
Outils pour suspendre
Crochet de boucherie sur rail.

    Le crochet de levage (crochet d'attelage, crochet de traction), qui peut s'engager dans un anneau ou la boucle d'un cordage afin de tirer ou de lever une charge.
    le crochet, ou hameçon, utilisé pour la pêche.
    le crochet de boucherie (ou esse), destinée à la suspension des quartiers de viande.

Outils textiles
Crochets de bottines.

    le crochet destiné au travail des mailles dans les activités textiles (confection de dentelles, de napperons, de vêtements ou d'amigurumis).
    le crochet de bottine, ou tire-bouton, une tige métallique achevée en crochet et fixée dans un manche, qui permet de passer les boutons des bottines (ou des gants) dans les boutonnières.

Autres outils

    le Crochet utilisé pour ouvrir des serrures.
    le crochet de fixation, qui permet de garder un objet solidaire d'un support :
        le crochet de gouttière suspend la gouttière à l'égout du toit.
        le crochet d'ardoise ou de tuile retient les éléments de couverture sur leur support, etc.
    le crochet, une prothèse rudimentaire (cf. le capitaine Crochet).

Sciences et techniques
Biologie

    Les crochets des serpents venimeux sont des dents creuses leur servant à injecter le venin (Ce sont des canines modifiées[réf. nécessaire]).
    Dans la denture du cheval, les crochets sont les canines des chevaux mâles (les juments en ont rarement).

Mathématiques

    Le crochet de Lie est une loi de composition interne sur un espace vectoriel.
    Le crochet de Poisson, un cas particulier du crochet de Lie.

Typographie

    Les crochets typographiques sont les signes [ et ].
    Le crochet est un diacritique souscrit.
    Le crochet en chef est un diacritique suscrit utilisé en vietnamien.
    Dans la norme Unicode, on utilise le terme de crochet (anglais : hook) pour désigner un jambage descendant recourbé vers la gauche, incorporé à certains glyphes. Exemples : Ӄ (lettre majuscule cyrillique ka crochet), ҕ (lettre minuscule cyrillique ghé crochet médian).

Culture et société
Langue courante

    « Faire un crochet » signifie prendre un chemin détourné.

Légendes et littérature

    Dans les légendes populaires de la Champagne et de l'Ardenne, Marie Crochet (ou Marie-Crochet) est un être imaginaire, doué d'ubiquité, qui happe et noie les enfants qui s'approchent trop de l'eau.
    Le capitaine Crochet est un personnage du roman Peter Pan.

Artisanat

    En architecture, un crochet est un ornement en forme de touffe de feuilles ou de fruits1.
    Dans l'artisanat textile, le crochet, par extension, désigne aussi la technique du crochet et l'ouvrage réalisé avec un crochet.

Radiophonie

    Certains concours de chant radiophoniques sont appelés radio-crochets.

Sports et arts martiaux

    Dans les arts martiaux chinois, le crochet du tigre est une arme utilisée en Wushu.
    En boxe, le crochet est un coup de poing circulaire.

Patronyme

Crochet est un nom de famille notamment porté par :

    Jules Crochet, un pionnier de l’aviation sanitaire ;
    Marcel Crochet, un ingénieur belge, professeur à l'université catholique de Louvain ;
    Roger Crochet, un entrepreneur français, fondateur de la chaîne de discothèques Macumba.

Notes et références

    Crochet [archive] par J. Justin Storck.

Catégories :

    HomonymieHomonymie de patronyme

[+]
Menu de navigation

    Non connecté
    Discussion
    Contributions
    Créer un compte
    Se connecter

    Article
    Discussion

    Lire
    Modifier
    Modifier le code
    Voir l’historique

Rechercher

    Accueil
    Portails thématiques
    Article au hasard
    Contact

Contribuer

    Débuter sur Wikipédia
    Aide
    Communauté
    Modifications récentes
    Faire un don

Outils

    Pages liées
    Suivi des pages liées
    Téléverser un fichier
    Pages spéciales
    Lien permanent
    Informations sur la page
    Élément Wikidata
    Citer cette page

Imprimer / exporter

    Créer un livre
    Télécharger comme PDF
    Version imprimable

Dans d’autres langues

    Català
    Español
    Euskara

Modifier les liens

    La dernière modification de cette page a été faite le 5 septembre 2019 à 12:54.
    Droit d'auteur : les textes sont disponibles sous licence Creative Commons attribution, partage dans les mêmes conditions ; d’autres conditions peuvent s’appliquer. Voyez les conditions d’utilisation pour plus de détails, ainsi que les crédits graphiques. En cas de réutilisation des textes de cette page, voyez comment citer les auteurs et mentionner la licence.
    Wikipedia® est une marque déposée de la Wikimedia Foundation, Inc., organisation de bienfaisance régie par le paragraphe 501(c)(3) du code fiscal des États-Unis.

    Politique de confidentialité
    À propos de Wikipédia
    Avertissements
    Contact
    Développeurs
    Déclaration sur les témoins (cookies)
    Version mobile

    Wikimedia Foundation	
    Powered by MediaWiki	

-----COURAGEUX-----

Wiki Loves Monuments : photographiez un monument historique, aidez Wikipédia et gagnez !
En apprendre plus
Courageux
Sauter à la navigationSauter à la recherche
Page d'aide sur l'homonymie. Cette page d’homonymie répertorie les différents sujets et articles partageant un même nom.
Courageux, de courage, peut désigner :


Sommaire
1	Cinéma
2	Littérature
3	Navires
4	Voir aussi
Cinéma
Un homme courageux, une comédie muette sortie en 1924 ;
Capitaines courageux, un film de Victor Fleming sorti en 1937 ;
Littérature
Courageux, un roman de Jack Campbell paru en 2009 ;
Navires
le Courageux Ce lien renvoie vers une page d'homonymie, le nom de plusieurs navires de la marine française ;
le HMS Courageux Ce lien renvoie vers une page d'homonymie, le nom de plusieurs navires de la Royal Navy
Voir aussi
courage (homonymie) Ce lien renvoie vers une page d'homonymie
Catégorie : Homonymie
>>>>>>> 77a4142662b616a94281a1f7f3aeea8981747e76
Menu de navigation
Non connectéDiscussionContributionsCréer un compteSe connecterArticleDiscussionLireModifierModifier le codeVoir l’historiqueRechercher
Rechercher dans Wikipédia
Accueil
Portails thématiques
Article au hasard
Contact
Contribuer
Débuter sur Wikipédia
Aide
Communauté
Modifications récentes
Faire un don
Outils
Pages liées
Suivi des pages liées
Téléverser un fichier
Pages spéciales
Lien permanent
Informations sur la page
Élément Wikidata
Citer cette page
<<<<<<< HEAD
Dans d’autres projets
Wikimedia Commons
=======
>>>>>>> 77a4142662b616a94281a1f7f3aeea8981747e76
Imprimer / exporter
Créer un livre
Télécharger comme PDF
Version imprimable

<<<<<<< HEAD
Dans d’autres langues
Brezhoneg
Català
Corsu
Deutsch
English
Español
Italiano
Português
Русский
43 de plus
Modifier les liens
La dernière modification de cette page a été faite le 20 août 2019 à 12:18.
Droit d'auteur : les textes sont disponibles sous licence Creative Commons attribution, partage dans les mêmes conditions ; d’autres conditions peuvent s’appliquer. Voyez les conditions d’utilisation pour plus de détails, ainsi que les crédits graphiques. En cas de réutilisation des textes de cette page, voyez comment citer les auteurs et mentionner la licence.
Wikipedia® est une marque déposée de la Wikimedia Foundation, Inc., organisation de bienfaisance régie par le paragraphe 501(c)(3) du code fiscal des États-Unis.
Politique de confidentialitéÀ propos de WikipédiaAvertissementsContactDéveloppeursDéclaration sur les témoins (cookies)Version mobileWikimedia FoundationPowered by MediaWiki
=======
Langues
Ajouter des liens
La dernière modification de cette page a été faite le 23 octobre 2015 à 09:45.
Droit d'auteur : les textes sont disponibles sous licence Creative Commons attribution, partage dans les mêmes conditions ; d’autres conditions peuvent s’appliquer. Voyez les conditions d’utilisation pour plus de détails, ainsi que les crédits graphiques. En cas de réutilisation des textes de cette page, voyez comment citer les auteurs et mentionner la licence.
Wikipedia® est une marque déposée de la Wikimedia Foundation, Inc., organisation de bienfaisance régie par le paragraphe 501(c)(3) du code fiscal des États-Unis.
Politique de confidentialitéÀ propos de WikipédiaAvertissementsContactDéveloppeursDéclaration sur les témoins (cookies)Version mobileWikimedia FoundationPowered by MediaWiki
>>>>>>> b64c11dd89d9ff3faf3860606ce9478bcf2aaa05
>>>>>>> 77a4142662b616a94281a1f7f3aeea8981747e76

