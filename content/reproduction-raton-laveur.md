+++
date = "2019-10-03T09:51:35+02:00"
title = "La reproduction chez les ratons baveurs"
draft = false
thumbnail = "racoon.png"

+++

# La reproduction chez les ratons laveurs



Juste avant le printemps, le raton laveur mâle, qui peut se reproduire dès sa deuxième année, se met à chercher une ou plusieurs femelles. Après l'accouplement, le mâle repart et la femelle reste seule pour mettre au monde, élever et protéger ses petits.

Après 63 jours de gestation, la maman ratonne met au monde entre 1 et 3 ratonneaux dans les régions du sud et entre 3 et 9 ratonneaux dans les régions du nord. Elle ne peut avoir qu'une seule portée par an.

À la naissance, les petits ont les yeux et les oreilles fermés et ne pèsent pas plus de 75 g. Leur maman les nourrit avec son lait. Au bout de 15 à 20 jours, les yeux et les oreilles s'ouvrent et, à 4 semaines, les dents de lait apparaissent. Les ratonneaux passent leur premier hiver avec leur mère et ne se dispersent qu’au début de l’été suivant.

