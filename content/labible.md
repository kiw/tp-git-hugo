+++
date = "2019-10-03T09:42:28+02:00"
title = "labible"
draft = true

+++

# La bible c'est coul

C'est la bible!

# le nouveau testament

## Les Evangiles

### Mathieu

1:1 Généalogie de Jésus Christ, fils de David, fils d'Abraham.

1:2 Abraham engendra Isaac; Isaac engendra Jacob; Jacob engendra Juda et ses frères;

1:3 Juda engendra de Thamar Pharès et Zara; Pharès engendra Esrom; Esrom engendra Aram;

1:4 Aram engendra Aminadab; Aminadab engendra Naasson; Naasson engendra Salmon;

1:5 Salmon engendra Boaz de Rahab; Boaz engendra Obed de Ruth;

1:6 Obed engendra Isaï; Isaï engendra David. Le roi David engendra Salomon de la femme d'Urie;

1:7 Salomon engendra Roboam; Roboam engendra Abia; Abia engendra Asa;

1:8 Asa engendra Josaphat; Josaphat engendra Joram; Joram engendra Ozias;

1:9 Ozias engendra Joatham; Joatham engendra Achaz; Achaz engendra Ézéchias;

1:10 Ézéchias engendra Manassé; Manassé engendra Amon; Amon engendra Josias;

1:11 Josias engendra Jéchonias et ses frères, au temps de la déportation à Babylone.

1:12 Après la déportation à Babylone, Jéchonias engendra Salathiel; Salathiel engendra Zorobabel;

1:13 Zorobabel engendra Abiud; Abiud engendra Éliakim; Éliakim engendra Azor;

1:14 Azor engendra Sadok; Sadok engendra Achim; Achim engendra Éliud;

1:15 Éliud engendra Éléazar; Éléazar engendra Matthan; Matthan engendra Jacob;

1:16 Jacob engendra Joseph, l'époux de Marie, de laquelle est né Jésus, qui est appelé Christ.

1:17 Il y a donc en tout quatorze générations depuis Abraham jusqu'à David, quatorze générations depuis David jusqu'à la déportation à Babylone, et quatorze générations depuis la déportation à Babylone jusqu'au Christ.

1:18 Voici de quelle manière arriva la naissance de Jésus Christ. Marie, sa mère, ayant été fiancée à Joseph, se trouva enceinte, par la vertu du Saint Esprit, avant qu'ils eussent habité ensemble.

1:19 Joseph, son époux, qui était un homme de bien et qui ne voulait pas la diffamer, se proposa de rompre secrètement avec elle.

1:20 Comme il y pensait, voici, un ange du Seigneur lui apparut en songe, et dit: Joseph, fils de David, ne crains pas de prendre avec toi Marie, ta femme, car l'enfant qu'elle a conçu vient du Saint Esprit;

1:21 elle enfantera un fils, et tu lui donneras le nom de Jésus; c'est lui qui sauvera son peuple de ses péchés.

1:22 Tout cela arriva afin que s'accomplît ce que le Seigneur avait annoncé par le prophète:

1:23 Voici, la vierge sera enceinte, elle enfantera un fils, et on lui donnera le nom d'Emmanuel, ce qui signifie Dieu avec nous.

1:24 Joseph s'étant réveillé fit ce que l'ange du Seigneur lui avait ordonné, et il prit sa femme avec lui.

1:25 Mais il ne la connut point jusqu'à ce qu'elle eût enfanté un fils, auquel il donna le nom de Jésus.

2:1 Jésus étant né à Bethléhem en Judée, au temps du roi Hérode, voici des mages d'Orient arrivèrent à Jérusalem,

2:2 et dirent: Où est le roi des Juifs qui vient de naître? car nous avons vu son étoile en Orient, et nous sommes venus pour l'adorer.

2:3 Le roi Hérode, ayant appris cela, fut troublé, et tout Jérusalem avec lui.

2:4 Il assembla tous les principaux sacrificateurs et les scribes du peuple, et il s'informa auprès d'eux où devait naître le Christ.

2:5 Ils lui dirent: A Bethléhem en Judée; car voici ce qui a été écrit par le prophète:

2:6 Et toi, Bethléhem, terre de Juda, Tu n'es certes pas la moindre entre les principales villes de Juda, Car de toi sortira un chef Qui paîtra Israël, mon peuple.

2:7 Alors Hérode fit appeler en secret les mages, et s'enquit soigneusement auprès d'eux depuis combien de temps l'étoile brillait.

2:8 Puis il les envoya à Bethléhem, en disant: Allez, et prenez des informations exactes sur le petit enfant; quand vous l'aurez trouvé, faites-le-moi savoir, afin que j'aille aussi moi-même l'adorer.

2:9 Après avoir entendu le roi, ils partirent. Et voici, l'étoile qu'ils avaient vue en Orient marchait devant eux jusqu'à ce qu'étant arrivée au-dessus du lieu où était le petit enfant, elle s'arrêta.

2:10 Quand ils aperçurent l'étoile, ils furent saisis d'une très grande joie.

2:11 Ils entrèrent dans la maison, virent le petit enfant avec Marie, sa mère, se prosternèrent et l'adorèrent; ils ouvrirent ensuite leurs trésors, et lui offrirent en présent de l'or, de l'encens et de la myrrhe.

2:12 Puis, divinement avertis en songe de ne pas retourner vers Hérode, ils regagnèrent leur pays par un autre chemin.

2:13 Lorsqu'ils furent partis, voici, un ange du Seigneur apparut en songe à Joseph, et dit: Lève-toi, prends le petit enfant et sa mère, fuis en Égypte, et restes-y jusqu'à ce que je te parle; car Hérode cherchera le petit enfant pour le faire périr.

2:14 Joseph se leva, prit de nuit le petit enfant et sa mère, et se retira en Égypte.

2:15 Il y resta jusqu'à la mort d'Hérode, afin que s'accomplît ce que le Seigneur avait annoncé par le prophète: J'ai appelé mon fils hors d'Égypte.

2:16 Alors Hérode, voyant qu'il avait été joué par les mages, se mit dans une grande colère, et il envoya tuer tous les enfants de deux ans et au-dessous qui étaient à Bethléhem et dans tout son territoire, selon la date dont il s'était soigneusement enquis auprès des mages.

2:17 Alors s'accomplit ce qui avait été annoncé par Jérémie, le prophète:

2:18 On a entendu des cris à Rama, Des pleurs et de grandes lamentations: Rachel pleure ses enfants, Et n'a pas voulu être consolée, Parce qu'ils ne sont plus.

2:19 Quand Hérode fut mort, voici, un ange du Seigneur apparut en songe à Joseph, en Égypte,

2:20 et dit: Lève-toi, prends le petit enfant et sa mère, et va dans le pays d'Israël, car ceux qui en voulaient à la vie du petit enfant sont morts.

2:21 Joseph se leva, prit le petit enfant et sa mère, et alla dans le pays d'Israël.

2:22 Mais, ayant appris qu'Archélaüs régnait sur la Judée à la place d'Hérode, son père, il craignit de s'y rendre; et, divinement averti en songe, il se retira dans le territoire de la Galilée,

2:23 et vint demeurer dans une ville appelée Nazareth, afin que s'accomplît ce qui avait été annoncé par les prophètes: Il sera appelé Nazaréen. 

3:1 En ce temps-là parut Jean Baptiste, prêchant dans le désert de Judée.

3:2 Il disait: Repentez-vous, car le royaume des cieux est proche.

3:3 Jean est celui qui avait été annoncé par Ésaïe, le prophète, lorsqu'il dit: C'est ici la voix de celui qui crie dans le désert: Préparez le chemin du Seigneur, Aplanissez ses sentiers.

3:4 Jean avait un vêtement de poils de chameau, et une ceinture de cuir autour des reins. Il se nourrissait de sauterelles et de miel sauvage.

3:5 Les habitants de Jérusalem, de toute la Judée et de tout le pays des environs du Jourdain, se rendaient auprès de lui;

3:6 et, confessant leurs péchés, ils se faisaient baptiser par lui dans le fleuve du Jourdain.

3:7 Mais, voyant venir à son baptême beaucoup de pharisiens et de sadducéens, il leur dit: Races de vipères, qui vous a appris à fuir la colère à venir?

3:8 Produisez donc du fruit digne de la repentance,

3:9 et ne prétendez pas dire en vous-mêmes: Nous avons Abraham pour père! Car je vous déclare que de ces pierres-ci Dieu peut susciter des enfants à Abraham.

3:10 Déjà la cognée est mise à la racine des arbres: tout arbre donc qui ne produit pas de bons fruits sera coupé et jeté au feu.

3:11 Moi, je vous baptise d'eau, pour vous amener à la repentance; mais celui qui vient après moi est plus puissant que moi, et je ne suis pas digne de porter ses souliers. Lui, il vous baptisera du Saint Esprit et de feu.

3:12 Il a son van à la main; il nettoiera son aire, et il amassera son blé dans le grenier, mais il brûlera la paille dans un feu qui ne s'éteint point.

3:13 Alors Jésus vint de la Galilée au Jourdain vers Jean, pour être baptisé par lui.

3:14 Mais Jean s'y opposait, en disant: C'est moi qui ai besoin d'être baptisé par toi, et tu viens à moi!

3:15 Jésus lui répondit: Laisse faire maintenant, car il est convenable que nous accomplissions ainsi tout ce qui est juste. Et Jean ne lui résista plus.

3:16 Dès que Jésus eut été baptisé, il sortit de l'eau. Et voici, les cieux s'ouvrirent, et il vit l'Esprit de Dieu descendre comme une colombe et venir sur lui.

3:17 Et voici, une voix fit entendre des cieux ces paroles: Celui-ci est mon Fils bien-aimé, en qui j'ai mis toute mon affection. 

4:1 Alors Jésus fut emmené par l'Esprit dans le désert, pour être tenté par le diable.

4:2 Après avoir jeûné quarante jours et quarante nuits, il eut faim.

4:3 Le tentateur, s'étant approché, lui dit: Si tu es Fils de Dieu, ordonne que ces pierres deviennent des pains.

4:4 Jésus répondit: Il est écrit: L'homme ne vivra pas de pain seulement, mais de toute parole qui sort de la bouche de Dieu.

4:5 Le diable le transporta dans la ville sainte, le plaça sur le haut du temple,

4:6 et lui dit: Si tu es Fils de Dieu, jette-toi en bas; car il est écrit: Il donnera des ordres à ses anges à ton sujet; Et ils te porteront sur les mains, De peur que ton pied ne heurte contre une pierre.

4:7 Jésus lui dit: Il est aussi écrit: Tu ne tenteras point le Seigneur, ton Dieu.

4:8 Le diable le transporta encore sur une montagne très élevée, lui montra tous les royaumes du monde et leur gloire,

4:9 et lui dit: Je te donnerai toutes ces choses, si tu te prosternes et m'adores.

4:10 Jésus lui dit: Retire-toi, Satan! Car il est écrit: Tu adoreras le Seigneur, ton Dieu, et tu le serviras lui seul.

4:11 Alors le diable le laissa. Et voici, des anges vinrent auprès de Jésus, et le servaient.

4:12 Jésus, ayant appris que Jean avait été livré, se retira dans la Galilée.

4:13 Il quitta Nazareth, et vint demeurer à Capernaüm, située près de la mer, dans le territoire de Zabulon et de Nephthali,

4:14 afin que s'accomplît ce qui avait été annoncé par Ésaïe, le prophète:

4:15 Le peuple de Zabulon et de Nephthali, De la contrée voisine de la mer, du pays au delà du Jourdain, Et de la Galilée des Gentils,

4:16 Ce peuple, assis dans les ténèbres, A vu une grande lumière; Et sur ceux qui étaient assis dans la région et l'ombre de la mort La lumière s'est levée.

4:17 Dès ce moment Jésus commença à prêcher, et à dire: Repentez-vous, car le royaume des cieux est proche.

4:18 Comme il marchait le long de la mer de Galilée, il vit deux frères, Simon, appelé Pierre, et André, son frère, qui jetaient un filet dans la mer; car ils étaient pêcheurs.

4:19 Il leur dit: Suivez-moi, et je vous ferai pêcheurs d'hommes.

4:20 Aussitôt, ils laissèrent les filets, et le suivirent.

4:21 De là étant allé plus loin, il vit deux autres frères, Jacques, fils de Zébédée, et Jean, son frère, qui étaient dans une barque avec Zébédée, leur père, et qui réparaient leurs filets.

4:22 Il les appela, et aussitôt ils laissèrent la barque et leur père, et le suivirent.

4:23 Jésus parcourait toute la Galilée, enseignant dans les synagogues, prêchant la bonne nouvelle du royaume, et guérissant toute maladie et toute infirmité parmi le peuple.

4:24 Sa renommée se répandit dans toute la Syrie, et on lui amenait tous ceux qui souffraient de maladies et de douleurs de divers genres, des démoniaques, des lunatiques, des paralytiques; et il les guérissait.

4:25 Une grande foule le suivit, de la Galilée, de la Décapole, de Jérusalem, de la Judée, et d'au delà du Jourdain. 
