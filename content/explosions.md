+++
date = "2019-10-03T09:50:47+02:00"
title = "explosions"
draft = false

+++

Les inquiétudes, légitimes, sur les conséquences de l'incendie de l'usine Lubrizol à Rouen ont servi de terreau à de nombreuses manipulations sur les réseaux sociaux tout au long du week-end, de la vidéo virale d'une gigantesque explosion aux faux communiqués sur l'air et l'eau potable. Comment les vérifier ? 
1. Une explosion venue d'ailleurs

Comme toujours lorsque des catastrophes visuellement impressionnantes se produisent, des internautes sautent sur l'occasion pour rediffuser des images anciennes et tenter de créer le buzz. A l'instar de la vidéo d'une effrayante explosion - de nuit donc difficilement identifiable - mais présentée par un internaute comme se déroulant sur le site Seveso normand. 

 
Capture d'écran Facebook réalisée le 28/09/2019

 
Pourquoi se méfier ? 

Postée la nuit de l'incendie, et surfant sur l'inquiétude des riverains, elle a cumulé plusieurs milliers de partages et au moins une centaine de milliers de vues en très peu de temps.

Difficile de détecter d'un premier coup d'oeil que ces images ne montrent pas Rouen, mais la vidéo est de très faible qualité, ce qui peut indiquer qu'elle a été téléchargée puis publiée à de nombreuses reprises sur une longue période, voyant ainsi sa qualité dégradée. 
Comment vérifier ?

En temps normal, en utilisant la recherche d'image inversée, grâce logiciel InVid que nous vous avons préconisé à plusieurs reprises, pour essayer de remonter jusqu'à la première occurence de la vidéo. Nous vous détaillons la procédure ici.

Problème, dans ce cas précis cette recherche ne donne pas de résultats satisfaisants. Pourquoi ? Parce l'image a été inversée, ce qui peut tromper les moteurs de recherche.
Capture d'écran Facebook réalisée le 28/09/2019
Capture d'écran Facebook réalisée le 28/09/2019

 

Il convient donc de tenter d'inverser à nouveau l'image, avant de relancer une recherche d'image inversée, et le tour est joué. 

La vidéo montre en réalité une explosion meurtrière survenue en août 2015 à Tianjin, en Chine. 
2. Alerte sur l'eau potable 

C'est sans doute l'eau qui a le plus fait parler d'elle tout au long du weekend. D'abord via des publications Facebook partagées au moins un millier de fois, et relayant une prétendue consigne du CHU de Rouen appelant à ne pas boire au robinet.
Capture d'écran Facebook réalisée le 28/09/2019
Pourquoi se méfier ? 

La capture d'écran est alarmiste, à grand renfort d'émoticônes, mais surtout elle ne met aucun lien pour prouver sa source. Cela ne veut pas dire qu'elle se trompe, mais c'est une raison suffisante pour douter.
Comment vérifier ?

En allant à la source justement. En allant fouiller les différents canaux de diffusion du CHU de Rouen : son site officiel, sa page Facebook, son compte Twitter.

Et justement, sur son compte Twitter, l'hôpital a lui-même démenti cette affirmation. 

    Nous sommes en lien direct avec la cellule de crise de
    @Prefet76
    : l'eau du robinet est potable.
    — CHU de Rouen (@CHURouen)
    September 26, 2019

Le cas des vidéos d'"eau noire"

Beaucoup plus virales encore, une photo d'un filet d'eau du robinet marron partagée près de 20.000 fois, et la vidéo d'une eau grisâtre coulant dans un lavabo, cumulant plus d'un million de vues, suscitent toutes les interrogations. 
Capture d'écran Twitter prise le 30/09/2019
Capture d'écran Twitter prise le 30/09/2019

 

Contactée par l'AFP, l'auteure de la vidéo explique que ses images -- vues plus d'un million de fois -- ont été tournées dimanche "à 21 heures" et ne plus avoir "d'eau noire pour le moment". 

Selon la préfecture de Seine-Maritime et l'Agence régionale de santé (ARS), qui dit n'avoir reçu que deux signalements lundi, "l’eau du robinet peut être consommée sans risque pour la santé" dans tout le département. 

Réagissant à ces mêmes images "montrant une eau grise ou marron (...) chez des habitants de l’agglomération de Rouen", la préfecture de Seine-Maritime et l'ARS Normandie ont réagi lundi dans un communiqué commun : "deux signalements ont été effectués (...) ce matin. L’ARS Normandie s’est rapidement rapprochée des collectivités concernées (...) pour mener des investigations : après vérification, ces signalements n’ont pas été confirmés".

"L'un de ces signalements concernait un robinet de jardin sur la commune de Saint-Pierre-de-Varangeville [située à une dizaine de kilomètres de Rouen], l'autre chez une personne âgée à Mont-Saint-Aignan", a expliqué à l'AFP Catherine Boutet, ingénieure sanitaire et responsable du pôle santé-environnement de l'ARS Normandie, précisant que "des prélèvements ont été faits pour voir s'il n'y a pas eu quelque chose malgré tout".

La responsable du pôle santé-environnement de l'ARS Normandie précise par ailleurs que le faible nombre de signalements tend à démontrer qu'il s'agit de problèmes "localisés à des habitations" et non liés au réseau d'eau potable.

Un numéro vert (0800 021 021) a été mis en place par la métropole de Rouen pour signaler toute anomalie sur le réseau d'eau potable. 

Retrouvez plus d'informations sur ces deux cas par ici. 
3. De l'importance de l'orthographe

"Des effets dangereux pour la santé à court terme ne sont pas à exclure", affirme un "communiqué" diffusé sur Facebook avec deux en-têtes très officiels de la préfecture de Seine-Maritime et de l'Agence régionale de Santé, qui met en garde contre une "toxicité aiguë" des fumées résultant de l'incendie, se dirigeant vers les Hauts-de-Seine. Il est partagé par de nombreux internautes. 