+++
title = "ZeldaBOTW"
draft = true
date = "2019-09-20T16:06:15+02:00"
thumbnail = "ZeldaBOTW.jpg"

+++

![ZeldaBOTW](https://static.mmzstatic.com/wp-content/uploads/2017/03/zelda-breath-of-the-wild-test-critique.jpg)

The Legend of Zelda: Breath of the Wild (ou simplement Breath of the Wild, parfois abrégé en BotW) est un jeu d'action-aventure développé par la division Nintendo EPD, assisté par Monolith Soft et publié par Nintendo le 3 mars 2017 sur Nintendo Switch lors du lancement de la console, ainsi que sur Wii U dont il est dernier jeu produit par Nintendo. C'est le dix-huitième jeu de la franchise The Legend of Zelda.

Dans un monde ouvert, Breath of the Wild propose d'incarner Link, amnésique, qui est réveillé après un long sommeil d'une centaine d'années par une mystérieuse voix qui le guide pour éliminer Ganon, le Fléau, et restaurer la paix dans le royaume d'Hyrule. Comme dans le premier jeu de la série, sorti en 1986, le joueur reçoit peu d'instructions et peut librement explorer le monde ouvert, en remplissant différentes tâches s'il le souhaite. Ces tâches consistent à résoudre des quêtes secondaires ainsi que des énigmes, pour obtenir des récompenses.

Un des objectifs majeurs de cet épisode fixé par l'équipe de développement est de repenser les conventions de la série. Le jeu propose ainsi de nouvelles mécaniques de gameplay avec l'implémentation d'un moteur physique complet pour la gestion des objets et de l'environnement, des visuels en haute définition ainsi que des voix-off pour les personnages principaux. Le monde de Breath of the Wild n'est pas structuré linéairement et a été conçu pour récompenser l'expérimentation. L'histoire peut ainsi être complétée d'une manière non linéaire.

À sa sortie, le jeu est encensé par la critique. Malgré quelques faiblesses techniques et des visuels en deçà de ceux des jeux des consoles concurrentes, il est ovationné pour sa direction artistique, la démesure de son monde ouvert et de son contenu, la qualité des musiques et l'ingéniosité des énigmes. Le jeu reçoit différentes récompenses en 2016 et 2017, dont le prix du jeu de l'année aux Game Awards 2017. Au 31 décembre 2018, le jeu s'est écoulé à 13,3 millions d'exemplaires, avec 11,7 millions sur Nintendo Switch et 1,6 million sur Wii U, faisant de lui le jeu le plus vendu de la série. Une suite directe est annoncée pendant l'E3 2019. 

<iframe width="853" height="480" src="https://www.youtube.com/embed/TqSd_Elcn5c?list=PLHWH_-U5CS7v16Y-mULir0t018JD1IwVS" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>