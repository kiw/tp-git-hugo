+++
date = "2019-09-20T13:57:50+02:00"
title = "La sensualité du saxophone"

+++

> Editeur : Une article une histoire

## Chapitre 1 : Le souffle

Geoffrey seul avec son saxophne, sans argent et sans famille, a du trouvez un moyen de gagnez de l'argent.
Son instruments a la main il parcourait les rues sans s'arreter de jouer.

## Chapitre 2 : La ruée

Geoffrey se mit a jouer sur les place public et dans le metro et ne vivait que grace au donation des passantes
Puis vient le jouer où un homme écouta Geoffrey jouer et fut intrigué par tant de talent
Cet homme ce nommait Aymeric H. il etait producteur et vu en Geoffrey un grand musicien.
Aymeric H. proposa à Geoffrey de le prendre sous son aile pour l'aider à se refaire financièrement.

## Chapitre 3 : L'apogée

Geoffrey s'entreina pendant des heures, des jours, des mois....
Puis quand il fut pret il sorta un album qui eu un succes phenomenale mais le reve ne cacher qu'un cauchemar.

## Chapitre 4 : La chute

Apres quelques temps Geoffrey redevint un inconnu, désesperer il fut l'ultime tentative : un concert au pied de la tour effiel
Mais personne ne s'arreta pour l'ecouter
Dans son chagrin il attrapa son saxophone et souffla jusqu'à dans l'intruments jusqu'à extinction de ces poumons.
